/*
 * RoboPlay for MSX
 * Copyright (C) 2023 by RoboSoft Inc.
 * 
 * keyboard.h
 */

#pragma once

#include <stdint.h>
#include <stdbool.h>

bool is_key_pressed_ESC();
bool is_key_pressed_LEFT();
bool is_key_pressed_RIGHT();
bool is_key_pressed_SPACE();

bool is_number_key_pressed(const uint8_t number);
