/*
 * RoboPlay for MSX
 * Copyright (C) 2023 by RoboSoft Inc.
 * 
 * roboplay.h
 */

#pragma once

#include <stdint.h>

typedef enum
{
    PLAY_MODE_SINGLE_FILE,
    PLAY_MODE_M3U,
    PLAY_MODE_M3U_RANDOM
} PLAY_MODE;

void show_info();

void check_arguments(char **argv, int argc);
void find_player_name();

void find_extension(char *name);

bool play_song();
