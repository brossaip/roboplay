/*
 * RoboPlay for MSX
 * Copyright (C) 2023 by RoboSoft Inc.
 *
 * memory.h
 */

#pragma once

#include <stdint.h>

#define MAX_NR_SEGMENTS 256
#define BUFFER_SIZE     256

extern char *g_current_song_name;
extern char *g_current_player_name;
extern char *g_m3u_name;
extern char *work_buffer;

uint16_t pre_allocate_ram_segments();
void restore_ram_segment();

void free_used_ram_segments();
uint8_t get_free_ram_segment();
void set_active_ram_segment(uint8_t segment_index);
