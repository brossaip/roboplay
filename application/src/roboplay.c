/*
 * RoboPlay for MSX
 * Copyright (C) 2023 by RoboSoft Inc.
 * 
 * roboplay.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
 
#include "support/inc/printf_simple.h"
#include "players/inc/player_interface.h"

#include "device.h"
#include "file.h"
#include "m3u.h"
#include "dos.h"
#include "memory.h"
#include "keyboard.h"
#include "player.h"
#include "roboplay.h"

inline void enable_interrupt() { __asm__("ei"); }
inline void disable_interrupt() { __asm__("di"); }

#define JIFFY 0xFC9E

static PLAY_MODE g_play_mode = PLAY_MODE_SINGLE_FILE;

char* g_extension;

static bool g_use_wildcard;
static bool g_use_fixed_player;

static uint32_t g_current_tick;
static uint32_t g_maximum_ticks;
static uint16_t g_maximum_time_in_s;

/*
 * show_info
 */
void show_info()
{
  printf_simple("RoboPlay v1.9 - MSX multi music format player\n\r");
  printf_simple("Created by RoboSoft Inc. in 2023\n\r");
}

/*
 * find_extension
 */
void find_extension(char *name)
{
  uint8_t found = strlen(name) - 1;
  while(name[found] != '.' && found > 0) found--;

  g_extension = &name[found + 1];
}

/*
 * check_arguments
 */
void check_arguments(char **argv, int argc)
{
  g_use_wildcard = false;
  g_use_fixed_player = false;

  if (!argc)
  {
    printf_simple("\n\rUsage:\n\r  RoboPlay [<player>.PLY] <song>.EXT\n\r");
    printf_simple("or\n\r  RoboPlay <name>.M3U [/R] [/T <seconds>]\n\r");
    printf_simple("\n\rPlayer name is optional, wildcards in the song name are allowed.");
    printf_simple("\n\rWhen no player is provided, the file extension is used for identification\n\r");
    printf_simple("\n\rUse the '/R' option to randomize the M3U playlist");
    printf_simple("\n\rUse the '/T' option to set maximum song play time in the M3U playlist\n\r");
    dos_exit(0);
  }

  for (uint8_t i = 0; i < strlen(argv[0]); i++)
  {
    argv[0][i] = toupper(argv[0][i]);
    if(argv[0][i] == '*' || argv[0][i] == '?') g_use_wildcard = true;
    if(argv[1][i] == '*' || argv[1][i] == '?') g_use_wildcard = true;
  } 
  
  if (argc > 1) 
  {  
    for (uint8_t i = 0; i < strlen(argv[1]); i++)
    {
      argv[1][i] = toupper(argv[1][i]);
    }
  }

  find_extension(argv[0]);

  if (!strcmp(g_extension, "M3U"))
  {
    /* Use M3U playlist */
    g_play_mode = PLAY_MODE_M3U;

    strcpy(g_m3u_name, argv[0]);
    
    for(uint8_t i = 1; i < argc; i++)
    {
      if (!strcmp(argv[i], "/R")) g_play_mode = PLAY_MODE_M3U_RANDOM;
      if (!strcmp(argv[i], "/T")) g_maximum_time_in_s = atoi(argv[++i]);
    }
  }
  else
  {
    if (argc == 1)
        strcpy(g_current_song_name, argv[0]);
    else
    {
     /* Player provided as argument*/
      g_use_fixed_player = true;
      get_program_path(g_current_player_name);
      strcat(g_current_player_name, argv[0]);

      strcpy(g_current_song_name, argv[1]);
    }
  }
}

/*
 * show_song_info
 */
void show_song_info()
{
    printf_simple("\n\r");
    printf_simple("Title       : %s\n\r", g_roboplay_interface->get_title());
    printf_simple("Author      : %s\n\r", g_roboplay_interface->get_author());
    printf_simple("Description : %s\n\r", g_roboplay_interface->get_description());

    if(g_roboplay_interface->get_subsongs() > 1)
    {
        printf_simple("Number of subsongs: %d\n\r", g_roboplay_interface->get_subsongs());
    }
        
    printf_simple("\nNow playing ...ESC to stop");

    if(g_roboplay_interface->get_subsongs() > 1)
    {
        printf_simple(", LEFT/RIGHT for subsong");
    }

     if (g_play_mode == PLAY_MODE_M3U || g_play_mode == PLAY_MODE_M3U_RANDOM || g_use_wildcard)
     {
        printf_simple(", SPACE for next song");
     }
    printf_simple("\n\r");
}

/*
 * play_song
 */
bool play_song()
{
  bool result = false;

  g_current_tick = 0;

  init_player();
  if(!g_roboplay_interface->load(g_current_song_name))
  {
    printf_simple("\n\rError: Not a valid file for this player\n\r");
    if(g_play_mode == PLAY_MODE_SINGLE_FILE) 
    {
      restore_ram_segment();
      dos_exit(_INTER);
    }
    else 
      return true;
  }

  show_song_info();

#ifndef __DEBUG
  disable_interrupt();
#endif

  uint8_t subsong = 0;
  g_roboplay_interface->rewind(subsong);

  set_refresh();
  g_maximum_ticks = g_maximum_time_in_s * (uint16_t)g_refresh;

  while(!is_key_pressed_ESC())
  {
    if((g_play_mode == PLAY_MODE_M3U || g_play_mode == PLAY_MODE_M3U_RANDOM) && (is_key_pressed_SPACE() || (g_maximum_ticks && g_current_tick > g_maximum_ticks)))
    {
      result = true;
      break;
    }

    if(g_use_wildcard && (is_key_pressed_SPACE() || (g_maximum_ticks && g_current_tick > g_maximum_ticks)))
    {
      result = true;
      break;
    }

    for(uint8_t i = 1; i < 7; i++)
    {
      if(is_number_key_pressed(i)) g_roboplay_interface->command(i);
    }

    if(!g_roboplay_interface->update())
    {
      g_roboplay_interface->rewind(subsong);
      set_refresh();
      g_maximum_ticks = g_maximum_time_in_s * (uint16_t)g_refresh;
    }

    if(g_roboplay_interface->get_subsongs() > 1)
    {
      if(is_key_pressed_RIGHT() && subsong < g_roboplay_interface->get_subsongs())
      {
        subsong++;
        g_roboplay_interface->rewind(subsong);
      }

      if(is_key_pressed_LEFT() && subsong > 0)
      {
        subsong--;
        g_roboplay_interface->rewind(subsong);
      }
    }

    g_current_tick++;
    wait_for_refresh();
  }

  reset_devices();
  free_used_ram_segments();

#ifndef __DEBUG
  enable_interrupt();
#endif

  return result;
}

 /*
  * main
  */
int main(char **argv, int argc)
{ 
  *g_current_song_name = '\0';
  *g_current_player_name = '\0';
  *g_m3u_name = '\0';

  g_maximum_time_in_s = 0;
  g_maximum_ticks = 0;
  g_current_tick  = 0;

  uint16_t* jiffy = (uint8_t *)JIFFY;
  srand(*jiffy);

  show_info();
  check_arguments(argv, argc);
  
  init_devices();

  if (g_play_mode == PLAY_MODE_M3U || g_play_mode == PLAY_MODE_M3U_RANDOM)
  {
      printf_simple("\n\rUsing M3U playlist: %s", g_m3u_name);
      if (g_play_mode == PLAY_MODE_M3U_RANDOM) printf_simple(", Randomized");
      if (g_maximum_time_in_s) printf_simple(", Max play time: %i [s]", g_maximum_time_in_s);
      printf_simple("\n\r");

      m3u_open_file(g_m3u_name);
  }

  printf_simple("\nTotal free memory found: %dK\n\r", pre_allocate_ram_segments() * 16);

  bool next_song = true;
  bool first_file = true;
  do
  {
    if(g_use_wildcard)
      next_song = find_next_file(first_file);
    else if(g_play_mode == PLAY_MODE_M3U)
      next_song = m3u_get_next_song(g_current_song_name);
    else if(g_play_mode == PLAY_MODE_M3U_RANDOM)
      m3u_get_random_song(g_current_song_name);

    if(next_song && !g_use_fixed_player)
    {
        find_extension(g_current_song_name);
        find_player_name();
    } 

    first_file = false;
  } while(next_song && play_song());

  if (g_play_mode == PLAY_MODE_M3U || g_play_mode == PLAY_MODE_M3U_RANDOM)
  {
      m3u_close_file();
  }

  restore_devices();
  restore_ram_segment();

  return 0;
}