/*
 * RoboPlay for MSX
 * Copyright (C) 2023 by RoboSoft Inc.
 *
 * psg.h
 */

#pragma once

#include <stdint.h>

void psg_reset();

void psg_write(const uint8_t reg, const uint8_t value);
uint8_t psg_read(const uint8_t reg);

void psg2_write(const uint8_t reg, const uint8_t value);
uint8_t psg2_read(const uint8_t reg);
