/*
 * RoboPlay for MSX
 * Copyright (C) 2023 by RoboSoft Inc.
 *
 * soundstar.h
 */

#pragma once

#include <stdint.h>
#include <stdbool.h>

bool soundstar_find_slot();
void soundstar_reset();

bool soundstar_detected();
uint8_t soundstar_get_slot();
uint8_t soundstar_get_major_version();
uint8_t soundstar_get_minor_version();

void soundstar_reset();

void soundstar_write(const uint8_t reg, const uint8_t value);

