/*
 * RoboPlay for MSX
 * Copyright (C) 2023 by RoboSoft Inc.
 *
 * zsm.c
 *
 * ZSM: Commander X16 zsound player
 */

#include <string.h>

#include "player_interface.h"
#include "zsm.h"

const uint8_t g_channel_offsets[] =
{
  0x00, 0x01, 0x02, 0x08, 0x09, 0x0a, 0x10, 0x11, 0x12
};

static uint8_t reg;
static uint8_t val;

static int8_t* fn_ptr; 

bool load(char *const file_name)
{  
  fn_ptr = (uint8_t *)DATA_SEGMENT_BASE;

  g_roboplay_interface->open(file_name, false);

  g_header = (ZSM_HEADER *)DATA_SEGMENT_BASE;
  g_roboplay_interface->read(g_header, sizeof(ZSM_HEADER));

  g_segment_index = 0;
  g_segment_list[g_segment_index++] = START_SEGMENT_INDEX;

  uint32_t data_size = g_header->pcm_offset[2];
  data_size <<= 8;
  data_size += g_header->pcm_offset[1];
  data_size <<= 8;
  data_size += g_header->pcm_offset[0];

  if(data_size > 0)
    data_size -= sizeof(ZSM_HEADER);
  else
    data_size = 0x400000;

  uint16_t page_left = DATA_SEGMENT_SIZE - sizeof(ZSM_HEADER);
  uint8_t* destination = (uint8_t*)DATA_SEGMENT_BASE + sizeof(ZSM_HEADER);

  uint16_t read_size = 0;
  uint16_t bytes_read = 0;
  do
  {
    read_size = (data_size < READ_BUFFER_SIZE) ? data_size : READ_BUFFER_SIZE;
    read_size = (read_size < page_left) ? read_size : page_left;

    /* It's not possible to read directly to non-primary mapper memory segments,
       so use a buffer inbetween. */
    bytes_read = g_roboplay_interface->read((void*)READ_BUFFER, read_size);
    memcpy(destination, (void*)READ_BUFFER, bytes_read);

    data_size -= bytes_read;
    destination += bytes_read;
    page_left -= bytes_read;
    if(page_left == 0)
    {
      g_segment_list[g_segment_index] = g_roboplay_interface->get_new_segment();
      g_roboplay_interface->set_segment(g_segment_list[g_segment_index++]);

      page_left = DATA_SEGMENT_SIZE;
      destination = (uint8_t*)DATA_SEGMENT_BASE;
    }
  } while(bytes_read && data_size > 0);

  g_roboplay_interface->close();

  
  /* Read the frequency conversion table */
  g_conv_table_segment_1 = g_roboplay_interface->get_new_segment();
  g_conv_table_segment_2 = g_roboplay_interface->get_new_segment();

  g_roboplay_interface->set_segment(g_conv_table_segment_1);

  g_roboplay_interface->open("X16.DAT", true);

  data_size = DATA_SEGMENT_SIZE;
  destination = (uint8_t *)DATA_SEGMENT_BASE;
  bytes_read = 0;
  do
  {
      /* It's not possible to read directly to non-primary mapper memory segments,
          so use a buffer inbetween. */
      bytes_read = g_roboplay_interface->read((void*)READ_BUFFER, READ_BUFFER_SIZE);
      if(bytes_read)
      {
          data_size -= bytes_read;
          memcpy(destination, (void*)READ_BUFFER, bytes_read);
          destination += bytes_read;
      }
  } while(bytes_read && data_size > 0);

  g_roboplay_interface->set_segment(g_conv_table_segment_2);
  data_size = DATA_SEGMENT_SIZE;
  destination = (uint8_t *)DATA_SEGMENT_BASE;
  bytes_read = 0;
  do
  {
      /* It's not possible to read directly to non-primary mapper memory segments,
          so use a buffer inbetween. */
      bytes_read = g_roboplay_interface->read((void*)READ_BUFFER, READ_BUFFER_SIZE);
      if(bytes_read)
      {
          data_size -= bytes_read;
          memcpy(destination, (void*)READ_BUFFER, bytes_read);
          destination += bytes_read;
      }
  } while(bytes_read && data_size > 0);

  g_roboplay_interface->close();

  return true;
}

bool update()
{
  if(g_delay_counter)
    g_delay_counter--;

  while(!g_delay_counter)
  {
    uint8_t data = get_next_data_byte();

    if(data < 0x40)
      psg_write(data);
    else if(data == 0x40)
      ext_cmd();
    else if (data < 0x80)
      fm_write(data);
    else if (data == 0x80)
      return false;
    else 
      g_delay_counter = (data & 0x7F);
  }

  return true;
}

void rewind(int8_t subsong)
{
  /* No subsongs in this format */
  subsong;

  g_segment_index = 0;
  g_roboplay_interface->set_segment(g_segment_list[g_segment_index]);
  g_song_data = (void*)(DATA_SEGMENT_BASE + sizeof(ZSM_HEADER));

  g_delay_counter = 0;

  for(uint8_t i = 0; i < NR_OF_PSG_CHANNELS; i++)
  {
    g_psg_channels[i].freq_low = 0;
    g_psg_channels[i].freq_high = 0;
    g_psg_channels[i].pan_volume = 0;
    g_psg_channels[i].wave_pulse_width = 0;

    g_opl_channels[i].volume      = 0;
    g_opl_channels[i].pan         = 0x30;
    g_opl_channels[i].wave        = 0xff;
    g_opl_channels[i].pulse_width = 0x3f;
  }
}

void command(const uint8_t id)
{
  /* No additional commmands supported */
  id;
}

float get_refresh()
{    
  return g_header->tick_rate;
}

uint8_t get_subsongs()
{
  return 0;
}

char* get_player_info()
{
  return "Commander X16 zsound player V1.0 by RoboSoft Inc.";
}

char* get_title()
{
  return "-";
}

char* get_author()
{
  return "-";
}

char* get_description()
{
  return "-";
}

uint8_t get_next_data_byte()
{
  uint8_t value = *g_song_data;

  g_song_data++;
  if(g_song_data == (void*)(DATA_SEGMENT_BASE + DATA_SEGMENT_SIZE))
  {
      g_song_data = (void*)DATA_SEGMENT_BASE;
      g_roboplay_interface->set_segment(g_segment_list[++g_segment_index]);
  }

  return value;
}

void psg_write(uint8_t data)
{
  uint8_t channel = (data >> 2);
  uint8_t psg_register = data & 0x03;

  uint8_t value = get_next_data_byte();

  switch(psg_register)
  {
    case FREQUENCY_LOW:
      g_psg_channels[channel].freq_low = value;
      update_frequency(channel);
      break;
    case FREQUENCY_HIGH:
      g_psg_channels[channel].freq_high = value;
      update_frequency(channel);
      break;
    case PAN_VOLUME:
      g_psg_channels[channel].pan_volume = value;
      g_opl_channels[channel].volume = 0x3F - (value & 0x3F);

      if(g_opl_channels[channel].wave == WAVEFORM_SAWTOOTH || g_opl_channels[channel].wave == WAVEFORM_TRIANGLE)
      {
        g_roboplay_interface->opl_write_fm_1(g_channel_offsets[channel] + 0x40, g_opl_channels[channel].volume >> 1);
        g_roboplay_interface->opl_write_fm_1(g_channel_offsets[channel] + 0x43, g_opl_channels[channel].volume >> 1);
      }
      else if(g_opl_channels[channel].wave == WAVEFORM_NOISE)
      {
        g_roboplay_interface->opl_write_fm_1(g_channel_offsets[channel] + 0x40, g_opl_channels[channel].volume >> 4);
        g_roboplay_interface->opl_write_fm_1(g_channel_offsets[channel] + 0x43, g_opl_channels[channel].volume >> 4);
      }
      else if(g_opl_channels[channel].wave == WAVEFORM_PULSE)
      {
        g_roboplay_interface->opl_write_fm_1(g_channel_offsets[channel] + 0x43, g_opl_channels[channel].volume);
      }
      g_roboplay_interface->opl_write_fm_1(0xC0 + channel, g_opl_channels[channel].pan & 0x0F + ((value & 0xC0) >> 2));

      break;
    case WAVE_PULSE_WIDTH:
      g_psg_channels[channel].wave_pulse_width = value;

      if((value & 0xC0) != g_opl_channels[channel].wave)
      {
        g_roboplay_interface->opl_write_fm_1(0xB0 + channel, 0x00);
        select_wave(channel, value & 0xC0 );
        update_frequency(channel);
      }

      g_opl_channels[channel].pulse_width = value & 0x3f;
      if(g_opl_channels[channel].wave == WAVEFORM_PULSE)
      {
        g_roboplay_interface->opl_write_fm_1(g_channel_offsets[channel] + 0x40, g_opl_channels[channel].pulse_width);
      }
      break;
  }
}

void update_frequency(uint8_t channel)
{
  uint16_t freq;
  if(g_opl_channels[channel].wave == WAVEFORM_NOISE)
  {
    freq = 3333;
  }
  else
  {
    freq = g_psg_channels[channel].freq_high << 8;
    freq += g_psg_channels[channel].freq_low;
  }

  g_roboplay_interface->set_segment(g_conv_table_segment_1);
  uint8_t fn_low = fn_ptr[freq];
  g_roboplay_interface->set_segment(g_conv_table_segment_2);
  uint8_t bl_fn_high = fn_ptr[freq];
  g_roboplay_interface->set_segment(g_segment_list[g_segment_index]); 

  g_roboplay_interface->opl_write_fm_1(0xA0 + channel, fn_low);
  g_roboplay_interface->opl_write_fm_1(0xB0 + channel, bl_fn_high + 0x20);
}

void select_wave(uint8_t channel, uint8_t waveform)
{
  uint8_t channel_offset = g_channel_offsets[channel];

  g_opl_channels[channel].wave = waveform;

  switch(waveform)
  {
    case WAVEFORM_PULSE:
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x20, 0x21);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x40, g_opl_channels[channel].pulse_width);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x60, 0xf0);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x80, 0x0e);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0xE0, 0x05);
      
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x23, 0x21);   
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x43, g_opl_channels[channel].volume);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x43, 0x3f);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x63, 0xf0);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x83, 0x0e);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0xE3, 0x06);

      g_opl_channels[channel].pan = (g_opl_channels[channel].pan & 0xf0) | 0x00;
      g_roboplay_interface->opl_write_fm_1(0xC0 + channel, g_opl_channels[channel].pan);
      break;
    case WAVEFORM_SAWTOOTH:
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x20, 0x21);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x40, g_opl_channels[channel].volume >> 1);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x60, 0xfe);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x80, 0x0e);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0xE0, 0x00);

      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x23, 0x21);    
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x43, g_opl_channels[channel].volume >> 1);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x63, 0xfe);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x83, 0x0e);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0xE3, 0x07);

      g_opl_channels[channel].pan = (g_opl_channels[channel].pan & 0xf0) | 0x09;
      g_roboplay_interface->opl_write_fm_1(0xC0 + channel, g_opl_channels[channel].pan);
      break;
    case WAVEFORM_TRIANGLE:
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x20, 0x21);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x40, g_opl_channels[channel].volume >> 1);      
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x60, 0xf0);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x80, 0xf0);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0xE0, 0x04);

      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x23, 0x21);     
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x43, g_opl_channels[channel].volume >> 1);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x63, 0xf0);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x83, 0xf0);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0xE3, 0x00);

      g_opl_channels[channel].pan = (g_opl_channels[channel].pan & 0xf0) | 0x07;
      g_roboplay_interface->opl_write_fm_1(0xC0 + channel, g_opl_channels[channel].pan);
      break;
    case WAVEFORM_NOISE:
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x20, 0xe5);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x40, g_opl_channels[channel].volume >> 2);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x60, 0xf0);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x80, 0x0e);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0xE0, 0x02);

      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x23, 0x21);    
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x43, g_opl_channels[channel].volume >> 2);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x63, 0xf0);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0x83, 0x0e);
      g_roboplay_interface->opl_write_fm_1(channel_offset + 0xE3, 0x02);

      g_opl_channels[channel].pan = (g_opl_channels[channel].pan & 0xf0) | 0x0e;
      g_roboplay_interface->opl_write_fm_1(0xC0 + channel, g_opl_channels[channel].pan);
      break;
  }
}

void ext_cmd()
{
  uint8_t command = get_next_data_byte();

  uint8_t channel = command >> 6;
  uint8_t count = command & 0x3f;

  for(uint8_t i = 0; i < count; i++)
    get_next_data_byte();
}

void fm_write(uint8_t data)
{
  for(uint8_t i = 0; i < (data & 0x3f); i++)
  {
    reg = get_next_data_byte();
    val = get_next_data_byte();

    g_roboplay_interface->opm_write(reg, val);
  }
}
