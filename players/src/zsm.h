/*
 * RoboPlay for MSX
 * Copyright (C) 2023 by RoboSoft Inc.
 *
 * zsm.h
 *
 * ZSM: Commander X16 zsound player
 */

#pragma once

#define NR_OF_PSG_CHANNELS 16

#define WAVEFORM_PULSE     0x00
#define WAVEFORM_SAWTOOTH  0x40
#define WAVEFORM_TRIANGLE  0x80
#define WAVEFORM_NOISE     0xC0

typedef struct
{
  uint8_t  magic_header[2];
  uint8_t  version;
  uint8_t  loop_point[3];
  uint8_t  pcm_offset[3];
  uint8_t  fm_channel_mask;
  uint16_t pgs_channel_mask;
  uint16_t tick_rate;
  uint16_t reserved;
} ZSM_HEADER;

typedef struct
{
  uint8_t freq_low;
  uint8_t freq_high;
  uint8_t pan_volume;
  uint8_t wave_pulse_width;
} ZSM_PSG_REGISTERS;

typedef enum
{
  FREQUENCY_LOW,
  FREQUENCY_HIGH,
  PAN_VOLUME,
  WAVE_PULSE_WIDTH
} ZSM_PSG_REGISTER_TYPE;

typedef struct
{
  uint8_t volume;
  uint8_t pan;
  uint8_t wave;
  uint8_t pulse_width;
} OPL_REGISTERS;


ZSM_HEADER *g_header;

static uint8_t g_segment_list[256];
static uint8_t g_segment_index;

static uint8_t g_conv_table_segment_1;
static uint8_t g_conv_table_segment_2;

static uint16_t g_delay_counter;
static uint8_t *g_song_data;

ZSM_PSG_REGISTERS g_psg_channels[NR_OF_PSG_CHANNELS];
OPL_REGISTERS     g_opl_channels[NR_OF_PSG_CHANNELS];

uint8_t get_next_data_byte();
void psg_write(uint8_t data);
void update_frequency(uint8_t channel);
void select_wave(uint8_t channel, uint8_t waveform);
void ext_cmd();
void fm_write(uint8_t data);